import React from 'react'
import Flex from '../components/Flex';
import mainImg from '../main.png';
import {MdPlace, MdArrowDropUp, MdHome, MdSearch, MdMenu} from 'react-icons/md'
import {FaAward, FaCircle, FaChartLine} from 'react-icons/fa'

export default function Home() {
    console.log('console')
    console.log('console2')
    console.log('console3')
    return (
        <Flex className="h-screen w-full flex-col relative">
            <header>
                <img className="w-full" src={mainImg} alt=""/>
            </header>
            <div className="my-8 pb-36">
                <h2 className="mx-4 my-0 font-bold">이 달의 HOT Issue</h2>
                <Flex className='mx-4 my-2 justify-between'>
                    <Flex className="font-bold text-lg items-center">매매가 상승 TOP3
                        <MdPlace className="text-blue-500 text-xl"/>부산
                    </Flex>
                    <p className="text-sm underline text-gray-400 cursor-pointer">랭킹 더보기</p>
                </Flex>
                <Flex className="p-1">
                    <Flex className="ranking-box">
                        <FaAward className="text-4xl text-gray-400 m-4"/>
                        <p className="text-xs">남구 용호동</p>
                        <p className="ranking-name">W</p>
                        <p className="ranking-size">전용 165.41m<sup>2</sup>(공급66형)</p>
                        <p className="ranking-price">매매 25억 5,000만원</p>
                        <Flex className="ranking-up">
                            <MdArrowDropUp className="text-3xl"/>
                            <p> 8억 7,500만원</p>
                        </Flex>
                    </Flex>
                    <Flex className="ranking-box">
                        <FaAward className="text-4xl text-yellow-400 m-4"/>
                        <p className="text-xs">남구 용호동</p>
                        <p className="ranking-name">W</p>
                        <p className="ranking-size">전용 165.41m<sup>2</sup>(공급66형)</p>
                        <p className="ranking-price">매매 25억 5,000만원</p>
                        <Flex className="ranking-up">
                            <MdArrowDropUp className="text-3xl"/>
                            <p> 8억 7,500만원</p>
                        </Flex>
                    </Flex>
                    <Flex className="ranking-box">
                        <FaAward className="text-4xl text-yellow-800 m-4"/>
                        <p className="text-xs">남구 용호동</p>
                        <p className="ranking-name">W</p>
                        <p className="ranking-size">전용 165.41m<sup>2</sup>(공급66형)</p>
                        <p className="ranking-price">매매 25억 5,000만원</p>
                        <Flex className="ranking-up">
                            <MdArrowDropUp className="text-3xl"/>
                            <p> 8억 7,500만원</p>
                        </Flex>
                    </Flex>
                </Flex>
                <br/>
                <Flex className="justify-end mx-4 text-sm text-gray-400 underline"> 투자분석 더보기</Flex>
                <Flex className="flex-wrap justify-center">
                    <div className="w-full bg-indigo-500 m-4 p-4 text-white rounded-md">
                        <p className="text-sm m-2">부산광역시 남구 용호동 954</p>
                        <p className="font-bold m-2">W 전용 134.43.m<sup>2</sup>(공급 54형)</p>
                        <p className="m-2 text-sm">10억 5,444만원 투자시 <span className="underline">순이익 9억 4,814만원</span></p>
                        <p className="m-2 font-bold text-xl">연수익률(IRR) 39.03%</p>
                    </div>
                    <Flex className="text-gray-400 text-xs">
                        <FaCircle className="mx-1" style={{fontSize: '0.5rem'}}/>
                        <FaCircle className="mx-1 text-blue-600" style={{fontSize: '0.5rem'}}/>
                        <FaCircle className="mx-1" style={{fontSize: '0.5rem'}}/>
                        <FaCircle className="mx-1" style={{fontSize: '0.5rem'}}/>
                    </Flex>
                </Flex>
            </div>
            <Flex className="h-screen absolute items-end">
                <Flex className="fixed border-t-2 w-full bg-white">
                    <Flex className="nav-bar text-blue-600">
                        <MdHome className="text-3xl"/>
                        홈
                    </Flex>
                    <Flex className="nav-bar text-gray-400">
                        <MdSearch className="text-3xl"/>
                        검색
                    </Flex>
                    <Flex className="nav-bar text-gray-400">
                        <FaChartLine className="text-3xl"/>
                        투자분석
                    </Flex>
                    <Flex className="nav-bar text-gray-400">
                        <MdMenu className="text-3xl"/>
                        메뉴
                    </Flex>
                </Flex>
            </Flex>
        </Flex>
    )
}
