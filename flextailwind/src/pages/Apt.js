import React from 'react'
import Flex from '../components/Flex'
import {FaChartLine, FaPlus} from 'react-icons/fa';
import {MdHome, MdSearch, MdMenu, MdChevronLeft} from 'react-icons/md';

export default function Apt() {
    console.log('console')
    return (
        <Flex className="flex-col h-screen w-full relative">
            <Flex className="items-cetner border-b-2 py-4">
                <MdChevronLeft className="text-3xl"/>
                <p className="text-xl flex-grow text-center font-bold">관심아파트</p>
            </Flex>
            <div className="my-8 mx-4 pb-36">
                <Flex className="justify-between">
                    <p>관심아파트</p>
                    <p className="underline text-gray-500 text-sm">편집하기</p>
                </Flex>
                <div className="mx-2 my-4">
                    <Flex className="flex-col my-8">
                        <p className="text-sm text-gray-500">
                            <button className="apt-btn" style={{fontSize: 'xx-small'}}>관심아파트</button>
                            서울특별시 강남구 압구정동
                        </p>
                        <p className="font-bold">현대 7차</p>
                        <p className="text-sm text-gray-500">전용 243.2m<sup>2</sup> (공급80형)</p>
                    </Flex>
                    <Flex className="flex-col my-8">
                        <p className="text-sm text-gray-500">
                            <button className="apt-btn" style={{fontSize: 'xx-small'}}>관심아파트</button>
                            서울특별시 강남구 압구정동
                        </p>
                        <p className="font-bold">현대 65동</p>
                        <p className="text-sm text-gray-500">전용 243.23m<sup>2</sup> (공급85형)</p>
                    </Flex>
                    <Flex className="flex-col my-8">
                        <p className="text-sm text-gray-500">
                            <button className="apt-btn" style={{fontSize: 'xx-small'}}>관심아파트</button>
                            서울특별시 강남구 도곡동
                        </p>
                        <p className="font-bold">타워팰리스 3차</p>
                        <p className="text-sm text-gray-500">전용 235.74m<sup>2</sup> (공급92형)</p>
                    </Flex>
                </div>
                <Flex className="justify-center items-center text-blue-600 text-lg">
                    <FaPlus className="mx-4 text-2xl"/>
                    <p>관심 아파트 추가하기</p>
                </Flex>
            </div>
            <Flex className="h-screen absolute items-end">
                <Flex className="fixed border-t-2 w-full bg-white">
                    <Flex className="nav-bar text-gray-400">
                        <MdHome className="text-3xl"/>
                        홈
                    </Flex>
                    <Flex className="nav-bar text-gray-400">
                        <MdSearch className="text-3xl"/>
                        검색
                    </Flex>
                    <Flex className="nav-bar text-gray-400">
                        <FaChartLine className="text-3xl"/>
                        투자분석
                    </Flex>
                    <Flex className="nav-bar text-blue-600">
                        <MdMenu className="text-3xl"/>
                        메뉴
                    </Flex>
                </Flex>
            </Flex>
        </Flex>
    )
}
