import React from 'react'
import Flex from '../components/Flex'
import {FaUserCircle, FaUserAlt, FaHeart, FaFileSignature,
        FaChartLine, FaAward, FaFileExport} from 'react-icons/fa';
import {MdChevronRight, MdHome, MdMenu, MdSearch} from 'react-icons/md';

export default function Menu() {
    console.log('console')
    console.log('console2')
    return (
        <Flex className="flex-col h-screen relative">
            <header className="m-4">
                <p className="text-2xl font-bold">메뉴</p>
                <Flex className="items-center text-gray-500 mx-4 my-2">
                    <FaUserCircle className="text-2xl mx-1"/>
                    <p>lily.jeon@zipfund.co</p>
                </Flex>
            </header>
            <div className="m-6 pb-36">
                <p className="text-gray-500 font-bold">나의 집비서</p>
                <div className="my-4">
                    <Flex className="menu-category">
                        <FaUserAlt className="mx-2 text-gray-400 text-xl"/> 마이페이지 
                        <p className="flex-grow"></p><MdChevronRight className="text-2xl"/>
                    </Flex>
                    <Flex className="menu-category">
                        <FaHeart className="mx-2 text-red-400 text-xl"/> 관심아파트 
                        <p className="flex-grow"></p><MdChevronRight className="text-2xl"/>
                    </Flex>
                    <Flex className="menu-category">
                        <FaFileSignature className="mx-2 text-blue-400 text-xl"/> 나의 투자분석 
                        <p className="flex-grow"></p><MdChevronRight className="text-2xl"/>
                    </Flex>
                </div>
                <hr className="my-6"/>
                <p className="text-gray-500 font-bold">아파트 찾기</p>
                <div className="my-4">
                    <Flex className="menu-category">
                        <MdSearch className="mx-2 text-gray-400 text-xl"/> 검색하기 
                        <p className="flex-grow"></p><MdChevronRight className="text-2xl"/>
                    </Flex>
                    <Flex className="menu-category">
                        <FaAward className="mx-2 text-red-400 text-xl"/> 
                        <Flex className="flex-col">
                            <p>아파트 추천받기</p>
                            <p className="text-xs text-gray-500">원하는 조건의 아파트 추천</p>
                        </Flex>
                        <p className="flex-grow"></p><MdChevronRight className="text-2xl"/>
                    </Flex>
                </div>
                <hr className="my-6"/>
                <p className="text-gray-500 font-bold">투자 분석</p>
                <div className="my-4">
                    <Flex className="menu-category">
                        <FaChartLine className="mx-2 text-green-400 text-xl"/> 
                        <Flex className="flex-col">
                            <p>수익률 분석</p>
                            <p className="text-xs text-gray-500">현재 매도했을 때의 수익률 분석</p>
                        </Flex>
                        <p className="flex-grow"></p><MdChevronRight className="text-2xl"/>
                    </Flex>
                    <Flex className="menu-category">
                        <FaFileExport className="mx-2 text-purple-500 text-xl"/> 
                        <Flex className="flex-col">
                            <p>매도비용 분석</p>
                            <p className="text-xs text-gray-500">앞으로 매도했을 때 발생 비용, 현금화 가능 금액 분석</p>
                        </Flex>
                        <p className="flex-grow"></p><MdChevronRight className="text-2xl"/>
                    </Flex>
                </div>
            </div>
            <Flex className="h-screen absolute items-end">
                <Flex className="fixed border-t-2 w-full bg-white">
                    <Flex className="nav-bar text-gray-400">
                        <MdHome className="text-3xl"/>
                        홈
                    </Flex>
                    <Flex className="nav-bar text-gray-400">
                        <MdSearch className="text-3xl"/>
                        검색
                    </Flex>
                    <Flex className="nav-bar text-gray-400">
                        <FaChartLine className="text-3xl"/>
                        투자분석
                    </Flex>
                    <Flex className="nav-bar text-blue-600">
                        <MdMenu className="text-3xl"/>
                        메뉴
                    </Flex>
                </Flex>
            </Flex>
        </Flex>
    )
}
