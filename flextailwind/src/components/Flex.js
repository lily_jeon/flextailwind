import React from 'react'

export default function Flex(props) {
    return (
        <div className={props.className} style={{display: 'flex'}}>
            { props.children }
        </div>
    )
}
