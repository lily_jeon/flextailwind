import './App.css';
import {Route} from 'react-router-dom';
import Home from './pages/Home';
import Menu from './pages/Menu';
import Apt from './pages/Apt';

function App() {
  return (
    <div>
      <Route path="/" component={Home} exact/>
      <Route path="/menu" component={Menu} />
      <Route path="/apt" component={Apt} />
    </div>
  );
}

export default App;
